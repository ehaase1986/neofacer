/**
 * 
 */
'use strict';

const repl = require('repl');
const utils = require('./utils');

exports.start = function (token, bot) {
	const r = repl.start(token);
	r.on('exit', () => {
		bot.destroy();
		console.log('Received "exit" event from repl!');
	});
	r.setupHistory('./.history', () => true);
	r.defineCommand('lc', () => bot.commands = utils.loadCommands()); // reload commands
	r.context.bot = bot;
	r.context.utils = utils;
	return r;
}

