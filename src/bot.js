/**
 * 
 */
'use strict';

const Discord = require('discord.js');
const bot = new Discord.Client();
const { loadCommands, splitOnce } = require('./utils');
const config = require('../config.json');

/**
 * Initialize enviroment defaults from config.json
 */
for (const [key,value] of Object.entries(config)) {
	if (process.env[key] === undefined)
		process.env[key] = value;
}

bot.once('ready', async () => {
	console.log("Connected as " + bot.user.tag);
	// bot.setTimeout(() => bot.user.setStatus('invisible'), 500);
});

bot.on('invalidated', () => {
	console.warn('Session invalidated, shutting down');
	process.exit();
});

bot.on('rateLimit', (data) => {
	console.error('Rate limit hit');
	console.error(data);
});

const PREFIX = process.env.PREFIX;
bot.commands = loadCommands();
bot.on('message', async (message) => {
	if (!message.content.startsWith(PREFIX) || message.author.bot)
		return;
	const [cmd, args] = splitOnce(message.content.slice(PREFIX.length), ' ');
	const lc = cmd.toLowerCase();
	if (!bot.commands[lc])
		return;
	await bot.commands[lc].execute(bot, message, args);
});

module.exports = bot;

