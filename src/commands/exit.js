/**
 * 
 */
'use strict';

const { formatPetMessage } = require('../utils.js');

module.exports = {
	name: 'exit',
	description: 'Fetch an image of a pet',
	aliases: [],
	async execute(client, message, args) {
		client.destroy();
	},
};