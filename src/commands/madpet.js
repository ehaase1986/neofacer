/**
 * 
 */
'use strict';

const { formatPetMessage } = require('../utils.js');

module.exports = {
	name: 'madpet',
	description: 'Fetch an image of a pet',
	aliases: [],
	async execute(client, message, args) {
		await message.channel.send(formatPetMessage(module.exports.name, args, '3/4.png'));
	},
};