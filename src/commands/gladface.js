/**
 * 
 */
'use strict';

const { formatPetMessage } = require('../utils.js');

module.exports = {
	name: 'gladface',
	description: 'Fetch an image of a pet',
	aliases: [],
	async execute(client, message, args) {
		await message.channel.send(formatPetMessage(module.exports.name, args, '1/6.png'));
	},
};