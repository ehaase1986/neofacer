/**
 * 
 */
'use strict';

const { formatPetMessage } = require('../utils.js');

module.exports = {
	name: 'sadface',
	description: 'Fetch an image of a pet',
	aliases: [],
	async execute(client, message, args) {
		await message.channel.send(formatPetMessage(module.exports.name, args, '2/6.png'));
	},
};