/**
 * 
 */
'use strict';

const { formatPetMessage } = require('../utils.js');

// bigglad http://pets.neopets.com/cpn/Adopter/1/4.png
// bigsad http://pets.neopets.com/cpn/Adopter/2/4.png
// bigmad http://pets.neopets.com/cpn/Adopter/3/4.png
// bigsick http://pets.neopets.com/cpn/Adopter/4/4.png
// lilglad http://pets.neopets.com/cpn/Adopter/1/6.png
// lilsad  http://pets.neopets.com/cpn/Adopter/2/6.png
// lilmad http://pets.neopets.com/cpn/Adopter/3/6.png
// lilsick http://pets.neopets.com/cpn/Adopter/4/6.png

module.exports = {
	name: 'sickpet',
	description: 'Fetch an image of a pet',
	aliases: [],
	async execute(client, message, args) {
		await message.channel.send(formatPetMessage(module.exports.name, args, '4/4.png'));
	},
};