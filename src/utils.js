/**
 * 
 */
'use strict';

const Discord = require('discord.js');
const fs = require('fs');

/**
 * 
 */
exports.clearTextChannel = async function (channel) {
	const messages = await channel.messages.fetch(null, false);
	await channel.bulkDelete(messages);
}

/**
 * 
 */
exports.loadCommands = function () {
	const commandFiles = fs.readdirSync('./src/commands').filter(file => file.endsWith('.js'));
	const commands = {};
	for (const file of commandFiles) {
		const path = `./commands/${file}`;
		delete require.cache[require.resolve(path)];

		const command = require(path);
		commands[command.name] = command;
		console.debug(`Registering command ${command.name} [${command.aliases}]`);
		for (const alias of command.aliases || []) {
			commands[alias] = command;
		}
	}
	return commands;
}

/**
 * 
 */
exports.splitOnce = function splitOnce(str, find = ' ') {
	const idx = str.indexOf(find);
	if (idx == -1)
		return [str];
	return [str.slice(0, idx), str.slice(idx + 1)];
}

/**
 * 
 */
exports.waitStream = async function waitStream(stream) {
	return new Promise((res, rej) => {
		stream.on("finish", x => res(x));
		stream.on("error", err => rej(err));
	});
}

/**
 * 
 */
exports.formatPetMessage = function formatPetMessage(cmd, args, urlSuffix) {
	const { PREFIX } = process.env.PREFIX;
	if (!args || !args.length)
		return `Missing parameter, expected: \`${PREFIX}${cmd} PETNAME\``;
	const petname = args.toLowerCase();
	const prettyName = petname.slice(0, 1).toUpperCase() + petname.slice(1);
	const newMessage = new Discord.MessageEmbed()
		.setTimestamp()
		.setTitle(prettyName)
		.setDescription(`([Petpage](http://www.neopets.com/~${prettyName})) ([Lookup](http://neopets.com/petlookup.phtml?pet=${prettyName}))`)
		.setImage(`http://pets.neopets.com/cpn/${prettyName}/${urlSuffix}`);
	;
	return newMessage;
}