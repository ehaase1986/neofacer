## Building dockerfile
docker build -t username/discord-bot-neofacer .

## Running build
docker run --rm --name discord-bot-neofacer -d username/discord-bot-neofacer

### Using --restart you can make the bot auto-restart
docker run --restart unless-stopped --name discord-bot-neofacer username/discord-bot-neofacer

### Using --rm you can automatically discard the container when it exits (These options are mutally exclusive)
docker run --rm --name discord-bot-neofacer username/discord-bot-neofacer