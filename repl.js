/**
 *
 */
'use strict';

const bot = require('./src/bot');
const repl = require('./src/repl');

bot.once('ready', async () => {	
	repl.start('> ', bot);
});

bot.login(process.env.TOKEN);