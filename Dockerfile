#########################################################
# (Optional) deploy bot via docker!
#########################################################
FROM node:12-alpine

# Install dependencies
COPY package*.json ./
RUN npm ci --only=production

# Copy the rest of the code
COPY . .

# This bot will always launch as node main.js
CMD ["node", "main.js"]